CONTENTS OF THIS FILE
---------------------
  
* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------

This project integrates EuPlatesc.ro into the Drupal Commerce payment and checkout systems. 
The provider response can have three status codes: completed, canceled and pending. 
For every transaction, the customer should be notified via email address.

REQUIREMENTS
------------
This module requires the following modules:

* Drupal Commerce (https://www.drupal.org/project/commerce)